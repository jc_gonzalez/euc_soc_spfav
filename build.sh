#!/bin/bash
##############################################################################
# File       : build.sh - Build script for SPF AV
# Domain     : SPFAV
# Version    : 3.0
# Date       : 2020/05/06
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Build script for SPF AV
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

#-- This script path and name ----------------------------------------
SCRIPTPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SCRIPTNAME=$(basename "${BASH_SOURCE[0]}")
CURDIR=$(pwd)

cd ${SCRIPTPATH}

#-- Create build folder if non-existing ------------------------------
echo "Preparing . . ."
mkdir -p build/{qpfav,qslog}.dir/{moc,ui,obj,rcc}

#-- Ensure QT5 is available ------------------------------------------
echo "Checking that QT5 is available . . ."
QM=${QMAKE:-qmake-qt5}
HAS_QT5=$(${QM} -version|awk '/Using Qt version/{v=$4; if (v > "5.0") {print "OK";} else {print "NOK";}}')

if [ "$HAS_QT5" = "OK" ]; then
    ${QM} ./qpfav.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug
else
    echo "Qt 5.x is needed! Exiting."
    exit 1
fi

#-- Build ------------------------------------------------------------
echo "Building . . ."
( echo "# -*- compilation -*-"; make $* ) 2>&1 | tee build.log

#-- Return to original directory -------------------------------------
cd ${CURDIR}
echo "Done."
exit 0
