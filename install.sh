#!/bin/bash
##############################################################################
# File       : install.sh - Install script for SPF AV
# Domain     : SPFAV
# Version    : 3.0
# Date       : 2020/05/06
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Install script for SPF AV
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

#-- This script path and name ----------------------------------------
SCRIPTPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SCRIPTNAME=$(basename "${BASH_SOURCE[0]}")
CURDIR=$(pwd)

cd ${SCRIPTPATH}

#-- Install ------------------------------------------------------------
echo "Installing . . ."

TARGET_BINDIR=$HOME/.local/bin
TARGET_LIBDIR=$HOME/.local/lib

SRC_BASEDIR=${SCRIPTPATH}/build

cp ${SRC_BASEDIR}/qslog.dir/lib* ${TARGET_LIBDIR}/
cp ${SRC_BASEDIR}/qpfav.dir/qpfav ${TARGET_BINDIR}/spfav

IP=$(ifconfig |grep inet|grep netmask|grep -v "\.1 "|awk '{print $2;}')

echo "Updating environment settings script . . ."
cat <<EOF>>${HOME}/env_spf.sh

## SPF AV Settings
export LD_LIBRARY_PATH=${TARGET_LIBDIR}:\$LD_LIBRARY_PATH
export PATH=${TARGET_BINDIR}:\$PATH
export CFG=${HOME}/spf/cfg/spf-cfg.SOC30.json
export IP=$IP
EOF

#-- Return to original directory -------------------------------------
cd ${CURDIR}
echo "Done."
exit 0
