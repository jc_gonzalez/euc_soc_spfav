/******************************************************************************
 * File:    logsyntaxhi.cpp
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.logsyntaxhi
 *
 * Last update:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declaration of the class LogSyntaxHighlighter
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/

#include "logsyntaxhi.h"

#include <QTextDocument>

#define EscRE(s)  QRegularExpression(QStringLiteral(s))

namespace SPF {

LogSyntaxHighlighter::LogSyntaxHighlighter(QTextDocument *parent )
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    //==== Category messages

    // TRACE
    traceFmt.setForeground(Qt::lightGray);
    rule.pattern = EscRE("(?:[\\[:]\\b(T|TRC|TRAC|TRACE)\\b[:\\]])([[:print:]]+)$");
    rule.format = traceFmt;
    highlightingRules.append(rule);
    
    // DEBUG
    debugFmt.setForeground(Qt::gray);
    rule.pattern = EscRE("(?:[\\[:]\\b(D|DBG|DEBUG)\\b[:\\]])([[:print:]]+)$");
    rule.format = debugFmt;
    highlightingRules.append(rule);
    
    // INFO
    infoFmt.setForeground(Qt::darkGreen);
    rule.pattern = EscRE("(?:[\\[:]\\b(I|INF|INFO|INFORMATION)\\b[:\\]])([[:print:]]+)$");
    rule.format = infoFmt;
    highlightingRules.append(rule);
    
    // WARNING
    warnFmt.setForeground(Qt::darkYellow);
    rule.pattern = EscRE("(?:[\\[:]\\b(W|WRN|WARN|WARNING|PROGRESS)\\b[:\\]])([[:print:]]+)$");
    rule.format = warnFmt;
    highlightingRules.append(rule);
    
    // ERROR
    errorFmt.setForeground(Qt::red);
    rule.pattern = EscRE("(?:[\\[:]\\b(E|ERR|ERROR)\\b[:\\]])([[:print:]]+)$");
    rule.format = errorFmt;
    highlightingRules.append(rule);
    
    // CRITICAL/FATAL
    fatalFmt.setBackground(Qt::red);
    fatalFmt.setForeground(Qt::white);
    rule.pattern = EscRE("(?:[\\[:]\\b(F|C|CRIT|FATAL|CRITICAL)\\b[:\\]])([[:print:]]+)$");
    rule.format = fatalFmt;
    highlightingRules.append(rule);
    
    //==== Categories

    // TRACE
    traceFmt.setBackground(Qt::lightGray);
    traceFmt.setForeground(Qt::gray);
    rule.pattern = EscRE("[\\[:]\\b(T|TRC|TRAC|TRACE)\\b[:\\]]");
    rule.format = traceFmt;
    highlightingRules.append(rule);
    
    // DEBUG
    debugFmt.setBackground(Qt::lightGray);
    debugFmt.setForeground(Qt::darkGray);
    rule.pattern = EscRE("[\\[:]\\b(D|DBG|DEBUG)\\b[:\\]]");
    rule.format = debugFmt;
    highlightingRules.append(rule);
    
    // INFO
    infoFmt.setBackground(Qt::green);
    infoFmt.setForeground(Qt::black);
    rule.pattern = EscRE("[\\[:]\\b(I|INF|INFO|INFORMATION)\\b[:\\]]");
    rule.format = infoFmt;
    highlightingRules.append(rule);
    
    // WARNING
    warnFmt.setBackground(Qt::yellow);
    warnFmt.setForeground(Qt::darkMagenta);
    rule.pattern = EscRE("[\\[:]\\b(W|WRN|WARN|WARNING|PROGRESS)\\b[:\\]]");
    rule.format = warnFmt;
    highlightingRules.append(rule);
    
    // ERROR
    errorFmt.setBackground(Qt::red);
    errorFmt.setForeground(Qt::white);
    rule.pattern = EscRE("[\\[:]\\b(E|ERR|ERROR)\\b[:\\]]");
    rule.format = errorFmt;
    highlightingRules.append(rule);
    
    // CRITICAL/FATAL
    fatalFmt.setBackground(Qt::black);
    fatalFmt.setForeground(Qt::red);
    fatalFmt.setFontWeight(QFont::Bold);
    rule.pattern = EscRE("[\\[:]\\b(F|C|CRIT|FATAL|CRITICAL)\\b[:\\]]");
    rule.format = fatalFmt;
    highlightingRules.append(rule);

    //==== Date

    dateFmt.setForeground(Qt::darkCyan);
    rule.pattern = EscRE("^\\b[0-9]+[-_/. ][0-9]+[-_/. ][0-9]+[T ]+"
                         "\\b[0-9]+:[0-9]+:[0-9]+\\b");
    rule.format = dateFmt;
    highlightingRules.append(rule);
    rule.pattern = EscRE("^\\b[0-9]+[-_/. ][0-9]+[-_/. ][0-9]+[T ]+"
                         "\\b[0-9]+:[0-9]+:[0-9]+.[0-9]+\\b");
    highlightingRules.append(rule);
}

void LogSyntaxHighlighter::highlightBlock(const QString &text)
{
    for (const HighlightingRule &rule : qAsConst(highlightingRules)) {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(text);
        while (matchIterator.hasNext()) {
            QRegularExpressionMatch match = matchIterator.next();
            setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
    }

    setCurrentBlockState(0);
}

}
