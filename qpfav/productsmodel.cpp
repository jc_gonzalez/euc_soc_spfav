/******************************************************************************
 * File:    productsmodel.cpp
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.productsmodel
 *
 * Version:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Provides object implementation for some declarations
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/

#include "productsmodel.h"
#include <iostream>
namespace SPF {

ProductsModel::ProductsModel(std::vector<std::string> & pTypes, 
                             int siz)
{
    defineProductStrings(pTypes, siz);
    defineQueries();
    
    activateQuery(0);
}

void ProductsModel::defineProductStrings(std::vector<std::string> & pTypes, 
                                         int siz)
{
    stringWithProductTypes = "";
    for (auto & s: pTypes) {
        std::string ss(s + "                                ");
        stringWithProductTypes += ss.substr(0, siz);
    }
    singleProdTypeLen = siz;
}

void ProductsModel::defineQueries()
{
    PQuery pq;
    
    //==== 0. Default
    /*
    pq.qstr = QString::fromStdString(
                "SELECT  "
                "    concat(p.product_type) as Signature, "
                "    concat(left(p.signature, 7), '.', "
                "                cast(position(cast(p.instrument_id as char) "
		        "                              in ' VNS') as char), '.', "
                "                to_char(position(p.product_type in '" + 
                stringWithProductTypes + "') / " + 
                std::to_string(singleProdTypeLen) + ", 'FM00MI'), '.', "
                "                p.product_version) as idx, "
                "    p.product_id as ID,  "
                "    p.product_type as Type,  "
                "    p.product_version as Version,  "
                "    p.product_size as Size,  "
                "    p.product_status_id as Status,  "
                "    p.creator_id as Creator,  "
                "    p.obsmode_id as ObsMode,  "
                "    p.start_time as Start,  "
                "    p.end_time as End,  "
                "    p.registration_time as RegTime,  "
                "    p.url as URL, "
                "    p.id as internal_id  "
                "FROM products_info p  "
                "@WHERE@ "
                "ORDER BY idx;");
    pq.hdrs = QStringList({"Signature", "Idx",
                "Product Id", "Type", "Version",
                "Size", "Status", "Creator", "Obs.Mode",
                "Start", "End", "Reg.Time", "URL", "IntID"});
    pq.skip = 0;
    pq.fld_ptype = 3;
    pq.fld_url = 12;
    query.append(pq);
    */
    
    //==== 1. Group by Product Type
    pq.qstr = QString::fromStdString(
                "SELECT  "
                "    p.product_type as ProductType, "
                "    p.product_id as ID,  "
                "    p.product_version as Version,  "
                "    p.product_size as Size,  "
                "    p.product_status_id as Status,  "
                "    p.creator_id as Creator,  "
                "    p.obsmode_id as ObsMode,  "
                "    p.start_time as Start,  "
                "    p.end_time as End,  "
                "    p.registration_time as RegTime,  "
                "    p.url as URL, "
                "    p.id as internal_id  "
                "FROM products_info p  "
                "@WHERE@ "
                "ORDER BY ProductType, ID;");
    pq.hdrs = QStringList({"Product Type", 
                "Product Id", "Version",
                "Size", "Status", "Creator", "Obs.Mode",
                "Start", "End", "Reg.Time", "URL", "IntID"});
    pq.skip = 0;
    pq.fld_ptype = 0;
    pq.fld_url = 10;
    query.append(pq);

    //==== 2. Group by Instrument
    pq.qstr = QString::fromStdString(
                "SELECT  "
                "    p.instrument_id as Instrument, "
                "    p.product_id as ID,  "
                "    p.product_type as ProductType, "
                "    p.product_version as Version,  "
                "    p.product_size as Size,  "
                "    p.product_status_id as Status,  "
                "    p.creator_id as Creator,  "
                "    p.obsmode_id as ObsMode,  "
                "    p.start_time as Start,  "
                "    p.end_time as End,  "
                "    p.registration_time as RegTime,  "
                "    p.url as URL, "
                "    p.id as internal_id  "
                "FROM products_info p  "
                "@WHERE@ "
                "ORDER BY ProductType;");
    pq.hdrs = QStringList({"Instrument", 
                "Product Id", "Product Type", "Version",
                "Size", "Status", "Creator", "Obs.Mode",
                "Start", "End", "Reg.Time", "URL", "IntID"});
    pq.skip = 0;
    pq.fld_ptype = 2;
    pq.fld_url = 11;
    query.append(pq);

    //==== 3. Group by Intrument and Obs.Id (+Exp)
    pq.qstr = QString::fromStdString(
                "SELECT  "
                "    concat(p.instrument_id, '.', p.obs_id) as Instrument_ObsId,"
                "    p.product_id as ID,  "
                "    p.product_type as Type,  "
                "    p.product_version as Version,  "
                "    p.product_size as Size,  "
                "    p.product_status_id as Status,  "
                "    p.creator_id as Creator,  "
                "    p.obsmode_id as ObsMode,  "
                "    p.start_time as Start,  "
                "    p.end_time as End,  "
                "    p.registration_time as RegTime,  "
                "    p.url as URL, "
                "    p.id as internal_id  "
                "FROM products_info p  "
                "@WHERE@ "
                "ORDER BY Instrument_ObsId;");
    pq.hdrs = QStringList({"Instrument+Obs.Id", 
                "Product Id", "Type", "Version",
                "Size", "Status", "Creator", "Obs.Mode",
                "Start", "End", "Reg.Time", "URL", "IntID"});
    pq.skip = 0;
    pq.fld_ptype = 2;
    pq.fld_url = 11;
    query.append(pq);

    //==== 4. Group by Signature
    pq.qstr = QString::fromStdString(
                "SELECT  "
                "    p.signature as Signature,"
                "    p.product_id as ID,  "
                "    p.product_type as Type,  "
                "    p.product_version as Version,  "
                "    p.product_size as Size,  "
                "    p.product_status_id as Status,  "
                "    p.creator_id as Creator,  "
                "    p.obsmode_id as ObsMode,  "
                "    p.start_time as Start,  "
                "    p.end_time as End,  "
                "    p.registration_time as RegTime,  "
                "    p.url as URL, "
                "    p.id as internal_id  "
                "FROM products_info p  "
                "@WHERE@ "
                "ORDER BY Signature;");
    pq.hdrs = QStringList({"Signature", 
                "Product Id", "Type", "Version",
                "Size", "Status", "Creator", "Obs.Mode",
                "Start", "End", "Reg.Time", "URL", "IntID"});
    pq.skip = 0;
    pq.fld_ptype = 2;
    pq.fld_url = 11;
    query.append(pq);
}

void ProductsModel::activateQuery(int i)
{
    PQuery & pq = query[i];

    defineQuery(pq.qstr);
    defineHeaders(pq.hdrs);
    skipColumns(pq.skip);

    numFldPType = pq.fld_ptype;
    numFldUrl = pq.fld_url;

    refresh();
}

int ProductsModel::getNumFldPType()
{
    return numFldPType;
}

int ProductsModel::getNumFldUrl()
{
    return numFldUrl;
}

}
