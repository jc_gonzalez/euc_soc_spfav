#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "darkpalette.h"
#include "dbmng.h"
#include "config.h"
#include "version.h"
#include "fmkavconn.h"

#include "configtool.h"
#include "dbbrowser.h"
#include "exttoolsdef.h"
#include "verbleveldlg.h"

#include "dlgqdtfilter.h"
#include "frm_filtview.h"
#include "prodfiltmodel.h"
#include "tskspecmodel.h"
#include "frm_localarchview.h"

#include "logsyntaxhi.h"

#include <iostream>
#include <sstream>

#include <QFileDialog>
#include <QMessageBox>
#include <QTimer>
#include <QtConcurrent/QtConcurrentRun>
#include <QNetworkRequest>
#include <QNetworkReply>

// Basic log macros
#define TMsg(s)  std::cerr << s << '\n';
#define DMsg(s)  std::cerr << s << '\n';
#define IMsg(s)  std::cerr << s << '\n';
#define WMsg(s)  std::cerr << s << '\n';
#define EMsg(s)  std::cerr << s << '\n';
#define FMsg(s)  std::cerr << s << '\n';

using Configuration::cfg;

namespace SPF {

// Status bar
const int MainWindow::MessageDelay = 2000;

// Sections in HMI
const int SectionArchive = 0;    
const int SectionTasks   = 1;  
const int SectionViews   = 2;   
const int SectionViewer  = 3;   
const int SectionFilter  = 4;   
const int SectionAlerts  = 5;   
const int SectionMonit   = 6;  
const int SectionLog     = 7;
const int SectionTools   = 8;  

// Template for pushTo/pullFromVoSpace request file
const QString MainWindow::VOSpaceURL =
    "https://vospace.esac.esa.int/vospace/";

inline void delay(int millisecondsWait)
{
    QEventLoop loop;
    QTimer t;
    t.connect(&t, &QTimer::timeout, &loop, &QEventLoop::quit);
    t.start(millisecondsWait);
    loop.exec();
}
    
//----------------------------------------------------------------------
// Constructor
//----------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent, QString & cfgFile, QString s) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    styleName(s),
    netMng(new QNetworkAccessManager())
{
    // Configure object
    configure(cfgFile);

    // Set UI
    ui->setupUi(this);
    completeUi();

    // Launch run thread
    //std::thread(&LocalArchiveView::run, this).detach();
    QTimer * refreshTimer = new QTimer(this);
    connect(refreshTimer, SIGNAL(timeout()), this, SLOT(run()));
    refreshTimer->start(1000);
}

//----------------------------------------------------------------------
// Destructor
//----------------------------------------------------------------------
MainWindow::~MainWindow()
{
    delete ui;
}

//----------------------------------------------------------------------
// Method: configure
// Rest of operations to setup UI completly
//----------------------------------------------------------------------
void MainWindow::configure(QString & cfgFile)
{
    // Load configuration
    cfg.load(cfgFile);
    getUserToolsFromSettings();

    // Initialize DB
    setDB();
}

//----------------------------------------------------------------------
// Method: completeUi
// Rest of operations to setup UI completly
//----------------------------------------------------------------------
void MainWindow::completeUi()
{
    // Create general GUI actions handler
    int ptypeIdx = LocalArchiveViewFieldNames.indexOf("Type");
    aHdl = new ActionsHandler(this, 0, ptypeIdx);

    // Initialize palette
    initPalette();

    // Update tool button ids
    int i = 0;
    for (auto & tbtn: {ui->tbtnArchive, ui->tbtnTasks, ui->tbtnViews,
		       ui->tbtnViewer, ui->tbtnFilter, ui->tbtnAlerts, 
		       ui->tbtnMonit, ui->tbtnLog, ui->tbtnTools}) {
        ui->btngrpNavigator->setId(tbtn, i++);
    }

    // Set verbosity level
    updateVerbLogLevel();

    // Local Archive panel
    initLocalArchiveView(ui->localArchView, aHdl);

    QStringList localArchViewModes({"By Product Type",
                                    "By Instrument",
                                    "By Instrument + Obs.Id.",
                                    "By Signature"});   
    ui->cboxLocalArchiveContentMode->addItems(localArchViewModes);
    ui->cboxLocalArchiveContentMode->setCurrentIndex(0);

    // Tasks panel
    ui->tasksView->setActionsHandler(aHdl);

    // Product viewers panel
    connect(ui->prodsViewer, SIGNAL(newProductViewerAvailable(QString)),
            this, SLOT(addToProdViewersList(QString)));
    connect(ui->lstvwProducts, &QListWidget::itemDoubleClicked,
            [this](QListWidgetItem *item) { 
                this->ui->prodsViewer->selectProductViewer(item->text());
                this->showSection(SectionViewer);
            });

    initProdViewFiltersFmk();

    // Local Archive Views panel
    connect(ui->lstvwViews, &QListWidget::itemDoubleClicked,
            [this](QListWidgetItem *item) {
                QString numTab = item->text().left(item->text().indexOf(":"));
                for (int k = 0; k < ui->tabwdgViews->count(); ++k) {
                    QString tabText = ui->tabwdgViews->tabText(k);
                    QString numInTab = tabText.left(tabText.indexOf(":"));
                    if (numInTab == numTab) {
                        ui->tabwdgViews->setCurrentIndex(k);
                        break;
                    }
                }
                this->showSection(SectionViews);
            });

    // Report Filters
    connect(ui->tbtnCreateRptFilter, SIGNAL(clicked()), this, SLOT(reportFiltering()));
    connect(ui->lstvwFilters, &QListWidget::itemDoubleClicked,
            [this](QListWidgetItem *item) {
                QString numTab = item->text().left(item->text().indexOf(":"));
                for (int k = 0; k < ui->tabwdgFilters->count(); ++k) {
                    QString tabText = ui->tabwdgFilters->tabText(k);
                    QString numInTab = tabText.left(tabText.indexOf(":"));
                    if (numInTab == numTab) {
                        ui->tabwdgFilters->setCurrentIndex(k);
                        break;
                    }
                }
                this->showSection(SectionFilter);
            });

    // Tools panel
    connect(ui->btnCfgTool, SIGNAL(clicked()), aHdl->acConfigTool, SLOT(trigger()));
    connect(ui->btnDBBrowser, SIGNAL(clicked()), aHdl->acBrowseDB, SLOT(trigger()));
    connect(ui->btnUserDefTools, SIGNAL(clicked()), aHdl->acExtTools, SLOT(trigger()));
    connect(ui->btnVerbosity, SIGNAL(clicked()), aHdl->acVerbosity, SLOT(trigger()));

    connect(ui->btnCfgTool_txt, SIGNAL(clicked()), aHdl->acConfigTool, SLOT(trigger()));
    connect(ui->btnDBBrowser_txt, SIGNAL(clicked()), aHdl->acBrowseDB, SLOT(trigger()));
    connect(ui->btnUserDefTools_txt, SIGNAL(clicked()), aHdl->acExtTools, SLOT(trigger()));
    connect(ui->btnVerbosity_txt, SIGNAL(clicked()), aHdl->acVerbosity, SLOT(trigger()));

    // Alerts panel
    ui->procAlertsView->setActionsHandler(aHdl);
    ui->sysAlertsView->setActionsHandler(aHdl);
    
    // Log panel
    logHl = new LogSyntaxHighlighter(ui->txtLog->document());
    
    QComboBox * cbx = ui->cboxLog;
    bool firstGrp = true;
    foreach (const QString & hostName, cfg.nodes) {
    	QString url = cfg.baseUrl[hostName];
        std::cerr << "Requesting log files from " << url.toStdString()
                  << "/log/logfiles\n";
    	QNetworkRequest request(QUrl(url + "/log/logfiles"));
    	QNetworkReply * reply = netMng->get(request);
    	connect(reply, &QNetworkReply::finished, 
                [this, reply, cbx, hostName, url, &firstGrp]
                {
                    // Once the reply is received, put the log file names in combo box
                    reply->deleteLater();
                    const QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
                    const QJsonObject obj = doc.object();
                    QJsonArray arr(obj["logfiles"].toArray());
                    if (arr.size() > 0) {
                        QVector<QString> varr;
                        foreach (const QJsonValue & s, arr) { varr.append(s.toString()); }
                        qSort(varr);
                        if (!firstGrp) {
                            cbx->insertSeparator(cbx->count());
                            this->newListWidgetSeparator(this->ui->lstvwLogs);
                        }
                        firstGrp = false; 
                        foreach (const QString & s, varr) {
                            QString labelItem = QString("Node %1 - %2").arg(hostName).arg(s);
                            cbx->addItem(labelItem,
                                         QString("%1/log/%2").arg(url).arg(s));
                            this->ui->lstvwLogs->addItem(labelItem);
                        }
                    }
                });
    }
    
    connect(ui->cboxLog, SIGNAL(currentIndexChanged(int)), this, SLOT(showRequestedLog(int)));
    connect(ui->btnUpdateLog, SIGNAL(clicked()), this, SLOT(updateRequestedLog()));
    connect(ui->btnWrapLog, SIGNAL(clicked()), this, SLOT(updateWrapLogMode()));

    connect(ui->lstvwLogs, &QListWidget::itemDoubleClicked,
            [this](QListWidgetItem *item) {
                int k = this->ui->lstvwLogs->row(item);
                this->showRequestedLog(k);
                this->showSection(SectionLog);
                ui->cboxLog->setCurrentIndex(k);
            });

    // Monitorization panel
    ui->treevwMonit->setModel(new TaskSpectraModel);

    // Get status from machines
    QTimer *machinesTimer = new QTimer(this);
    connect(machinesTimer, &QTimer::timeout, this, &MainWindow::getMachinesStatus);

    machinesTimer->singleShot(20, this, SLOT(getMachinesStatus()));
    machinesTimer->start(5000);
}

//----------------------------------------------------------------------
// SLOT: getMachinesStatus()
//----------------------------------------------------------------------
void MainWindow::getMachinesStatus()
{
    this->ui->treewdgMachines->clear();

    // - Get nodes info
    foreach (const QString & hostName, cfg.nodes) {        
    	QString url = cfg.baseUrl[hostName];
        QNetworkReply * reply = netMng->get(QNetworkRequest(QUrl(url + "/status")));
    	connect(reply, &QNetworkReply::finished, 
                [this, reply, hostName, url]
                {
                    reply->deleteLater();
                    // Once the reply is received, put the status data in stream
                    QByteArray ans = reply->readAll();
                    const QJsonDocument doc = QJsonDocument::fromJson(ans);
                    const QJsonObject obj = doc.object();
                    const QJsonObject machine(obj["machine"].toObject());
                    const QJsonArray load(machine["load"].toArray());
                    const QJsonValue uname(machine["uname"]);
                    QString sload1 = QString("%1").arg(load[0].toDouble(), 6, 'g', 2);
                    QString sload5 = QString("%1").arg(load[1].toDouble(), 6, 'g', 2);
                    QString sload15 = QString("%1").arg(load[2].toDouble(), 6, 'g', 2);
                    QString suname = uname.toString();

                    // Add it to Tree Widget
                    QStringList rowItems;
                    rowItems << hostName << url << sload1 << sload5 << sload15 << suname;
                    this->ui->treewdgMachines->addTopLevelItem(new QTreeWidgetItem(rowItems));
                    for (int i = 0; i < 6; i++) this->ui->treewdgMachines->resizeColumnToContents(i);
                });
    }
}

//----------------------------------------------------------------------
// Method: newListWidgetSeparator
// Adds a separator-like item to a QListWidget
//----------------------------------------------------------------------
void MainWindow::newListWidgetSeparator(QListWidget * lwdg)
{
    QListWidgetItem * sep = new QListWidgetItem;
    QSize sz(0, 3);
    sep->setSizeHint(sz);
    sep->setFlags(Qt::NoItemFlags);
    QFrame * frm = new QFrame;
    frm->setFrameShape(QFrame::HLine);
    lwdg->addItem(sep);
    lwdg->setItemWidget(sep, frm);
}

//----------------------------------------------------------------------
// Method: initLocalArchiveView
// Initialize the widget to show a view of the products in Local Archive
//----------------------------------------------------------------------
void MainWindow::initLocalArchiveView(LocalArchiveView * v,
                                      ActionsHandler * ah)
{
    v->init(this);
    setUToolTasks(v);
    
    connect(ui->cboxLocalArchiveContentMode, SIGNAL(currentIndexChanged(int)),
            v, SLOT(changeSelectRqst(int)));

    v->setAutoButtons(ui->cboxLocalArchAuto);
    connect(ui->tbtnRefresh, SIGNAL(clicked()), v, SLOT(arefresh()));
    connect(ui->tbtnExpand,  SIGNAL(clicked()), v, SLOT(aexpand()));
    connect(ui->tbtnCollapse, SIGNAL(clicked()), v, SLOT(acollapse()));
    connect(ui->tbtnResize, SIGNAL(clicked()), v, SLOT(aresize()));

    v->setActionsHandler(ah);
    connect(v, SIGNAL(openProductInViewer(QString)),
            ui->prodsViewer, SLOT(createNewViewer(QString)));
}

//----------------------------------------------------------------------
// Method: initProdViewFiltersFmk
// Initialize the macro-widget to build product view filters
//----------------------------------------------------------------------
void MainWindow::initProdViewFiltersFmk()
{
    // Filter initialisation
    FieldSet products;
    Field fld("product_id"          , STRING);   products[fld.name] = fld;
    fld = Field("product_type"      , STRING);   products[fld.name] = fld;
    fld = Field("product_version"   , STRING);   products[fld.name] = fld;
    fld = Field("signature"         , STRING);   products[fld.name] = fld;
    fld = Field("instrument_id"     , NUMBER);   products[fld.name] = fld;
    fld = Field("product_size"      , NUMBER);   products[fld.name] = fld;
    fld = Field("product_status_id" , NUMBER);   products[fld.name] = fld;
    fld = Field("creator_id"        , NUMBER);   products[fld.name] = fld;
    fld = Field("obsmode_id"        , NUMBER);   products[fld.name] = fld;
    fld = Field("start_time"        , DATETIME); products[fld.name] = fld;
    fld = Field("end_time"          , DATETIME); products[fld.name] = fld;
    fld = Field("registration_time" , DATETIME); products[fld.name] = fld;
    fld = Field("url"               , STRING);   products[fld.name] = fld;

    const QStringList & flds = LocalArchiveViewFields;
    const QStringList & hdrs = LocalArchiveViewFieldNames;

    ui->wdgFilters->setFields(products, flds, hdrs);
    ui->wdgFilters->setTableName("products_info");

    ui->wdgFilters->setVisible(true);
    connect(ui->wdgFilters, SIGNAL(filterIsDefined(QString,QStringList)),
            this, SLOT(setProductsFilter(QString,QStringList)));
    connect(ui->wdgFilters, SIGNAL(filterReset()),
            this, SLOT(restartProductsFilter()));

    isProductsCustomFilterActive = false;
}

//----------------------------------------------------------------------
// SLOT: setPreoductsFilter
//----------------------------------------------------------------------
void MainWindow::setProductsFilter(QString qry, QStringList hdr)
{
    (void)hdr;
    
    static int tabNum = 0;

    QString vwName = ui->wdgFilters->getViewName();
    
    // QAbstractItemDelegate * del = ui->treevwArchive->itemDelegate();
    // qobject_cast<DBTreeBoldHeaderDelegate*>(del)->setCustomFilter(true);
    // isProductsCustomFilterActive = true;

    FrmLocalArchView * fv = new FrmLocalArchView;
    fv->setSql(qry);

    tabNum++;
    QString tabName = QString("%1:%2").arg(tabNum).arg(vwName);
    int tabIdx = ui->tabwdgViews->addTab(fv, tabName);
    ui->tabwdgViews->setTabIcon(tabIdx, QIcon(":/img/table.png"));

    addToViewsList(tabName);
    
    QString toolTip(qry);
    toolTip.replace(" WHERE", " \nWHERE");
    ui->tabwdgViews->setTabToolTip(tabIdx, toolTip);
    ui->tabwdgViews->setTabsClosable(false);
    connect(ui->tabwdgViews,
            static_cast<void (QTabWidget::*)(int)>(&QTabWidget::tabCloseRequested),
            [=](int k){this->ui->tabwdgViews->removeTab(k);});
    connect(fv, &FrmLocalArchView::closeThisViewRequested,
            [=](QWidget * w){this->ui->tabwdgViews->removeTab(
                                this->ui->tabwdgViews->indexOf(w));});
}

//----------------------------------------------------------------------
// SLOT: restartProductsFilter
//----------------------------------------------------------------------
void MainWindow::restartProductsFilter()
{
    // productsModel->setCustomFilter(false);
    // QAbstractItemDelegate * del = ui->treevwArchive->itemDelegate();
    // qobject_cast<DBTreeBoldHeaderDelegate*>(del)->setCustomFilter(false);
    // productsModel->restart();
    // isProductsCustomFilterActive = false;
}

//----------------------------------------------------------------------
// Method: initPalette
// Initialize palete for the GUI (dark)
//----------------------------------------------------------------------
void MainWindow::initPalette()
{
    //DarkPalette darkPalette(this);
    //darkPalette.apply(styleName, "DarkBlue");
}

//----------------------------------------------------------------------
// Method: setDB
// Specify connection settings for internal SPF DB
//----------------------------------------------------------------------
void MainWindow::setDB()
{
    // Prepare DBManager
    if (QSqlDatabase::connectionNames().isEmpty()) {
        QString dbName   (cfg.db("name").toString()); // ( "qpfdb" );     //Config::DBName.c_str() );
        QString userName (cfg.db("user").toString()); // ( "eucops" );    //Config::DBUser.c_str() );
        QString password (cfg.db("pwd") .toString()); // ( "" );          //Config::DBPwd.c_str() );
        QString hostName (cfg.db("host").toString()); // ( "127.0.0.1" ); //Config::DBHost.c_str() );
        QString port     (cfg.db("port").toString()); // ( "5432" );      //Config::DBPort.c_str() );

        DBManager::DBConnection connection =
            { "QPSQL", dbName, userName, password, hostName, port.toInt() };

        DMsg("Adding DB connection");
        DBManager::addConnection("qpfdb", connection);
        DMsg("Adding DB connection - DONE");
    }

    FmkAVConnection::setDB(DBManager::getDB());

    statusBar()->showMessage(tr("SPF HMI Ready . . ."), MessageDelay);
}

//----------------------------------------------------------------------
// Method: getUserTools
// Returns a reference to the user defined tools map
//----------------------------------------------------------------------
void MainWindow::getUserTools(MapOfUserDefTools & u)
{
    if (! u.isEmpty()) { u.clear(); }

    foreach (QString k, userDefTools.keys()) { 
        QUserDefTool udt = userDefTools[k];
        u.insert(k, udt); 
    }
}

//----------------------------------------------------------------------
// Method: getUserToolsFromSettings
// Retrieves user defined tools from settings file
//----------------------------------------------------------------------
void MainWindow::getUserToolsFromSettings()
{
    userDefTools.clear();
    QJsonArray uts = cfg.userDefToolsArray();
    int numUdefTools = uts.size();
    for (int i = 0; i < numUdefTools; ++i) {
        QUserDefTool qudt;
        qudt.name       = uts.at(i).toObject()["name"].toString();
        qudt.desc       = uts.at(i).toObject()["description"].toString();
        qudt.exe        = uts.at(i).toObject()["executable"].toString();
        qudt.args       = uts.at(i).toObject()["arguments"].toString();
        qudt.prod_types = uts.at(i).toObject()["productTypes"].toString().split(",");
        std::cerr << qudt.name.toStdString() << ", " 
                  << qudt.desc.toStdString() << ", " 
                  << qudt.exe.toStdString() << ", " 
                  << qudt.args.toStdString() << ", " 
                  << std::endl;
        userDefTools[qudt.name] = qudt;
    }

    userDefProdTypes.clear();
    foreach (QJsonValue s, cfg.products()["productTypes"].toArray()) {
        userDefProdTypes.append(s.toString());
    }
}

//----------------------------------------------------------------------
// Method: putUserToolsToSettings
// Retrieves user defined tools from settings file
//----------------------------------------------------------------------
void MainWindow::putUserToolsToSettings()
{
    /*
     TODO
     */
}

//----------------------------------------------------------------------
// Method: getProductTypes
// Provide the list (vector) of product type names
//----------------------------------------------------------------------
void MainWindow::getProductTypes(std::vector<std::string> & pTypes, int & siz)
{
    siz = 0;
    pTypes.clear();
    for (auto & s : cfg.products("productTypes").toArray().toVariantList()) {
        QString item = s.toString();
        //pTypes.push_back(item.replace("_LOG", "-LOG").toStdString());
        pTypes.push_back(item.toStdString());
        if (item.length() > siz) { siz = item.length(); }
    }
}

//----------------------------------------------------------------------
// Method: quitSPFAV
// Quits the application
//----------------------------------------------------------------------
void MainWindow::quitSPFAV()
{
    qApp->quit();
}

//----------------------------------------------------------------------
// Method: processPath
// Specify a folder and process the products contained therein
//----------------------------------------------------------------------
void MainWindow::processPath()
{
    QString folderName = QFileDialog::getExistingDirectory(this,
            tr("Process products in folder..."));
    if (! folderName.isEmpty()) {
        QtConcurrent::run(this, &MainWindow::processProductsInPath, folderName);
    }
}

//----------------------------------------------------------------------
// Method: processProductsInPath
// Specify a folder and process the products contained therein
//----------------------------------------------------------------------
void MainWindow::processProductsInPath(QString folder)
{
    // Get entire list (down the tree) of products in folder
    QStringList files;
    getProductsInFolder(folder, files);

    foreach (const QString & fi, files) {
        QFileInfo fs(fi);
        QString oldName(fs.absoluteFilePath());
        QString newName(cfg.storage.inbox + "/" + fs.fileName());
        QString newNamePart(cfg.storage.inbox + "/../" + fs.fileName() + ".part");
        QFile::copy(oldName, newNamePart);
        QFile::rename(newNamePart, newName);
        std::cerr << "Getting file " << newName.toStdString() << std::endl;
        delay(3000);
    }
}

//----------------------------------------------------------------------
// Method:
// Obtain all the product files under the path
//----------------------------------------------------------------------
void MainWindow::getProductsInFolder(QString & path, QStringList & files, bool recursive)
{
    QDir dir(path);
    QFileInfoList allEntries = dir.entryInfoList(QDir::Files |
            QDir::NoSymLinks | QDir::NoDotAndDotDot,
            QDir::Time | QDir::DirsLast);
    foreach (const QFileInfo & fi, allEntries) {
        QString absPath = fi.absoluteFilePath();
        std::cerr << "FILE_IN_FOLDER: " << absPath.toStdString() << '\n';
        if (fi.isDir()) {
            if (recursive) {
                getProductsInFolder(absPath, files, recursive);
            }
        } else {
            bool isProduct = true;
            if (isProduct) { files << absPath; }
        }
    }
}

#ifndef QT_NO_CLIPBOARD
//----------------------------------------------------------------------
// Method: cut
//----------------------------------------------------------------------
void MainWindow::cut()
{
}

//----------------------------------------------------------------------
// Method: copy
//----------------------------------------------------------------------
void MainWindow::copy()
{
}

//----------------------------------------------------------------------
// Method: paste
//----------------------------------------------------------------------
void MainWindow::paste()
{
}
#endif

//----------------------------------------------------------------------
// Method: about
// Reads configuration file
//----------------------------------------------------------------------
void MainWindow::about()
{
#ifndef BUILD_ID
#define BUILD_ID ""
#endif

    QString buildId(BUILD_ID);
    if (buildId.isEmpty()) {
        char buf[20];
        sprintf(buf, "%ld", (long)(time(0)));
        buildId = QString(buf);
    }

    QMessageBox::about(this, tr("About " APP_NAME),
                       tr("This is the " APP_PURPOSE " v " APP_RELEASE "\n"
                          APP_COMPANY "\n"
                          APP_DATE " - Build ") + buildId);
}

//----------------------------------------------------------------------
// Method: showConfigTool
// Shows configuration tool window
//----------------------------------------------------------------------
void MainWindow::showConfigTool()
{
    static ConfigTool cfgTool(0);

    cfgTool.prepare(userDefTools, userDefProdTypes);
    if (cfgTool.exec()) {
        TMsg("Updating user tools!");
        cfgTool.getExtTools(userDefTools);
        setUToolTasks();
        ui->lblVerbosity->setText(cfg.general("logLevel").toString());
    }
}

//----------------------------------------------------------------------
// Method: showDBBrowser
// Shows System Database Browser
//----------------------------------------------------------------------
void MainWindow::showDBBrowser()
{
    DBBrowser dlg;

    dlg.exec();
}

//----------------------------------------------------------------------
// Method: showExtToolsDef
// Shows external toos definition window
//----------------------------------------------------------------------
void MainWindow::showExtToolsDef()
{
    ExtToolsDef dlg;
    dlg.initialize(userDefTools, userDefProdTypes);
    if (dlg.exec()) {
        dlg.getTools(userDefTools);
        storeQUTools2Cfg(userDefTools);
        setUToolTasks();
    }
}

//----------------------------------------------------------------------
// Method: showVerbLevel
// Shows verbosity level selector dialog
//----------------------------------------------------------------------
void MainWindow::showVerbLevel()
{
    VerbLevelDlg dlg;
    dlg.setVerbosityLevel(cfg.general("logLevel").toString());

    if (dlg.exec()) {
        //int minLvl = dlg.getVerbosityLevelIdx();
        //Log::setMinLogLevel((Log::LogLevel)(minLvl));
	QString newLogLevel = dlg.getVerbosityLevelName();
        ui->lblVerbosity->setText(newLogLevel);
	cfg.general()["logLevel"] = newLogLevel;
	setVar("log_level", "", newLogLevel);
        //hmiNode->sendMinLogLevel(dlg.getVerbosityLevelName().toStdString());
        statusBar()->showMessage(tr("Setting Verbosity level to ") +
                                dlg.getVerbosityLevelName(), 2 * MessageDelay);
    }
}

//----------------------------------------------------------------------
// Method: convertQUTools2UTools
// Convert Qt map of user def tools to std map
//----------------------------------------------------------------------
void MainWindow::storeQUTools2Cfg(MapOfUserDefTools qutmap)
{
    QJsonArray uts;
    QMap<QString, QUserDefTool>::const_iterator it  = qutmap.constBegin();
    auto end = qutmap.constEnd();
    while (it != end) {
        const QUserDefTool & t = it.value();
        QJsonObject ut = { {"name", t.name},
                           {"description", t.desc},
                           {"executable", t.exe},
                           {"arguments", t.args},
                           {"productTypes", t.prod_types.join(",")} };
        uts.append(ut);
        ++it;
    }

    cfg.data().remove("userDefTools");
    cfg.data().insert("userDefTools", uts); 
}

//----------------------------------------------------------------------
// Method: setUToolTasks
// Set map of user defined tasks
//----------------------------------------------------------------------
void MainWindow::setUToolTasks(LocalArchiveView * v)
{
    aHdl->acUserTools.clear();
    foreach (QString key, userDefTools.keys()) {
        const QUserDefTool & udt = userDefTools.value(key);
        QAction * ac = new QAction(key, v);
        ac->setStatusTip(udt.desc);
        if (v != nullptr) connect(ac, SIGNAL(triggered()), v, SLOT(openWith()));
        aHdl->acUserTools[key] = ac;
    }
}

//----------------------------------------------------------------------
// Method: addToProdViewersList
// Add viewer
//----------------------------------------------------------------------
void MainWindow::addToProdViewersList(QString name)
{
    ui->lstvwProducts->addItem(name);
    showSection(SectionViewer);
}

//----------------------------------------------------------------------
// Method: addToViewsList
// Add local archive view
//----------------------------------------------------------------------
void MainWindow::addToViewsList(QString name)
{
    ui->lstvwViews->addItem(name);
    showSection(SectionViews);
}

//----------------------------------------------------------------------
// Method: addToRptFiltList
// Add local archive view
//----------------------------------------------------------------------
void MainWindow::addToRptFiltList(QString name)
{
    ui->lstvwFilters->addItem(name);
    showSection(SectionFilter);
}

//----------------------------------------------------------------------
// Method: showSection()
// Add viewer
//----------------------------------------------------------------------
void MainWindow::showSection(int sec)
{
    ui->stckMain->setCurrentIndex(sec);
}

//----------------------------------------------------------------------
// Method: getVar()
// Get variable value from DB
//----------------------------------------------------------------------
QString MainWindow::getVar(QString var, QString filter)
{
    QString flt("");

    if (!filter.isEmpty()) { 
	flt = " AND filter=" + filter; 
    } else {
	filter = "''";
    }

    QString qryStr("SELECT var_value FROM qpf_vars WHERE var_name='" + 
		      var + "'" + flt);

    // Get state
    QSqlQuery qry(qryStr, DBManager::getDB());
    if (qry.next()) {
        return qry.value(0).toString();
    }   

    return QString("");
}

//----------------------------------------------------------------------
// Method: setVar()
// Set variable in DB
//----------------------------------------------------------------------
void MainWindow::setVar(QString var, QString filter, QString new_value)
{
    QString flt("");

    if (!filter.isEmpty()) { flt = "AND filter='" + filter + "' "; }

    QString qryStr("WITH upsert AS (UPDATE qpf_vars "
		   "SET var_value='" + new_value + "' "
		   "WHERE var_name='" + var + "' " + flt + 
		   "RETURNING *) "
		   "INSERT INTO qpf_vars (var_name, filter, var_value) "
		   "SELECT '" + var + "', '" + filter + "', '" + new_value + "' "
		   "WHERE NOT EXISTS (SELECT * FROM upsert)");

    // Get state
    QSqlQuery qry(qryStr, DBManager::getDB());
}

//----------------------------------------------------------------------
// Method: run()
// Run loop
//----------------------------------------------------------------------
void MainWindow::run()
{
    updateState();
    updateVerbLogLevel();
}

//----------------------------------------------------------------------
// Method: updateStatee
//----------------------------------------------------------------------
void MainWindow::updateState()
{
    // Get state
    static QString qryStr("select node_state from node_states "
			  "where node_name='master'");
    QSqlQuery qry(qryStr, DBManager::getDB());
    if (qry.next()) {
        QString newStateName = qry.value(0).toString();
	if (newStateName != stateName) {
	    stateName = newStateName;
	    ui->lblState->setState(stateName);
	}
    }   
}

//----------------------------------------------------------------------
// Method: updateVerbLogLevel
//----------------------------------------------------------------------
void MainWindow::updateVerbLogLevel()
{
    QString cfgLogLevel = getVar("log_level", "");
    ui->lblVerbosity->setText(cfgLogLevel);
}

//----------------------------------------------------------------------
// Method: reportFiltering
// Create new filtered view by creating query to request report info
//----------------------------------------------------------------------
void MainWindow::reportFiltering()
{
    // Generate checks
    // This looks into the products table in the DB, and retrieves the list of
    // diagnostics with possible entries and values
    QSqlDatabase & db = DBManager::getDB();
    if (! db.open()) {
        QMessageBox::warning(this, tr("Problem with database"),
			     tr("Cannot access database!"),
			     QMessageBox::Ok, QMessageBox::Ok);
        return;
    }

    std::cerr << "report filtering...\n";
    
    QVector<QStringList> completeListOfChecks;

    // First, retrieve CCD diagnostics
    QSqlQuery("refresh materialized view prodfilt_checks_ccd;", db).exec();
    QSqlQuery query("select * from prodfilt_checks_ccd;", db);
    while (query.next()) {
        QStringList fields;
        fields << query.value(0).toString()
               << query.value(1).toString()
               << query.value(2).toString()
               << query.value(3).toString();
        completeListOfChecks.append(fields);
    }

    // Second, retrieve entire file diagnostics
    QSqlQuery("refresh materialized view prodfilt_checks_file;", db).exec();
    QSqlQuery query2("select * from prodfilt_checks_file;", db);
    while (query2.next()) {
        QStringList fields;
        fields << query2.value(0).toString()
               << query2.value(1).toString();
        completeListOfChecks.append(fields);     
    }

    // Then, get list of IDs of selected products (if any)
    QStringList ids;
    ids << "1" << "23" << "456";

    // ... and build list of product types
    QStringList prodTypes;
    for (auto & s : cfg.products("productTypes").toArray().toVariantList()) {
        prodTypes << s.toString();
    }

    // Finally, create and show dialog
    DlgQdtFilter d(completeListOfChecks, 0);
    d.setProductsList(prodTypes);
    d.setCurrentSelection(ids);

    if (! d.exec()) { return; }

    // Get results and create new model
    QString qryName, qryDef;
    d.getQry(qryName, qryDef);

    // Create view and new tab, and show view in the tab
    QString viewName(qryName);
    FrmFiltView * filtView = new FrmFiltView;
    QTreeView * newView = filtView->initialize(qryName, qryDef);
    ProductsFilterModel * prodFiltModel = new ProductsFilterModel(qryDef);
    newView->setModel(prodFiltModel);
    newView->setSortingEnabled(true);

    connect(filtView, SIGNAL(queryStringChanged(QString)),
            prodFiltModel, SLOT(changeQuery(QString)));

    int tabIdx = ui->tabwdgFilters->addTab(filtView, viewName);
    ui->tabwdgFilters->setTabIcon(tabIdx, QIcon(":/img/table.png"));

    QString toolTip(qryDef);
    toolTip.replace(" WHERE", " \nWHERE");
    ui->tabwdgFilters->setTabToolTip(tabIdx, toolTip);
    ui->tabwdgFilters->setTabsClosable(true);

    addToRptFiltList(viewName);
}

//----------------------------------------------------------------------
// Method: showRequestedLog
// Show selected log file
//----------------------------------------------------------------------
void MainWindow::showRequestedLog(int idx)
{
    QTextEdit * tlog = ui->txtLog;
    
    QNetworkRequest request(QUrl(ui->cboxLog->itemData(idx).toString()));
    QNetworkReply * reply = netMng->get(request);
    connect(reply, &QNetworkReply::finished, this, [this, tlog, reply] {
            reply->deleteLater();
            tlog->setPlainText(reply->readAll()); 
            QTextCursor cursor = tlog->textCursor();
            cursor.movePosition(QTextCursor::End);
            tlog->setTextCursor(cursor);
            tlog->ensureCursorVisible();
    });
    
    ui->lstvwLogs->setCurrentRow(idx);
}

//----------------------------------------------------------------------
// Method: updateRequestedLog
// Update log file
//----------------------------------------------------------------------
void MainWindow::updateRequestedLog()
{
    showRequestedLog(ui->cboxLog->currentIndex());
}

//----------------------------------------------------------------------
// Method: updateRequestedLog
// Update log file
//----------------------------------------------------------------------
void MainWindow::updateWrapLogMode()
{
    ui->txtLog->setLineWrapMode(ui->btnWrapLog->isChecked() ?
                                QTextEdit::WidgetWidth : QTextEdit::NoWrap);
}


const QStringList MainWindow::LocalArchiveViewFields {"product_id",
        "product_type", "product_version", "signature",
        "instrument_id", "product_size", "product_status_id", "creator_id",
        "obsmode_id", "start_time", "end_time", "registration_time", "url"};
const QStringList MainWindow::LocalArchiveViewFieldNames {"Product Id",
        "Type", "Version", "Signature", "Instrument",
        "Size", "Status", "Creator", "Obs.Mode", "Start", "End",
        "Reg.Time", "URL"};

}
