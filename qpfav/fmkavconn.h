/******************************************************************************
 * File:    fmkavconn.h
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.fmkavconn
 *
 * Last update:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declaration of FmkAVConn for SPF HMI
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/
#ifndef FMKAVCONN_H
#define FMKAVCONN_H

#include <QtSql>

#include "cmd.h"

namespace SPF {

class FmkAVConnection : public QObject {

    Q_OBJECT

public:
    //explicit FmkAVConnection(QObject *parent = 0);
    ~FmkAVConnection();

public:
    static void setDB(QSqlDatabase & aDB);

    static void addICmd(QString src, QString tgt, QString cmd);
    static bool getICmd(QString cmd, bool removeCmd = false);
    static void rmICmd(QString cmd);

    static void sendProcHdlCmd(SubjectId subj, QString subjName, SubcmdId subCmd);

private:
    FmkAVConnection();

private:
    static QSqlDatabase       db;
};

}

#endif // FMKAVCONN_H
