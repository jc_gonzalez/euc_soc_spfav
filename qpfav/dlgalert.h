/******************************************************************************
 * File:    dlgalert.h
 *          Declaration of class DlgAlert
 *
 * Domain:  SPF.qpfgui.dlgalert
 *
 * Last update:  2.0
 *
 * Date:    2016-11-03
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declaration of class DlgAlert
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <ChangeLog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/

#ifndef DLGALERT_H
#define DLGALERT_H

#include <QDialog>
#include <QDateTime>

#include "alert.h"

namespace Ui {
class DlgAlert;
}

namespace SPF {

class DlgAlert : public QDialog
{
    Q_OBJECT

public:
    explicit DlgAlert(QWidget *parent = Q_NULLPTR);
    ~DlgAlert();

    void setAlert(Alert & alert);

private:
    Ui::DlgAlert *ui;
};
}

#endif // DLGALERT_H
