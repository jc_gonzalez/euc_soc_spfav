// -*- C++ -*-

#ifndef CMD_H
#define CMD_H

#include <QString>

//-- Command Identifiers ---------------------

typedef QString CmdDescriptor;

#undef T

#define TLISTOF_CMD_IDS                              \
        T(INIT),                                     \
        T(SESSION),                                  \
        T(QUIT),                                     \
        T(PING),                                     \
        T(STATES),                                   \
        T(PROCHDL),                                  \
        T(CONFIG),                                   \
        T(REPROC), 

#define T(x) CMD_ ## x
enum CmdId { TLISTOF_CMD_IDS };
#undef T

#define T(x) QString( #x )
const CmdDescriptor CmdName[] = { TLISTOF_CMD_IDS };
#undef T

const CmdDescriptor CmdInit     (CmdName[CMD_INIT]);
const CmdDescriptor CmdSession  (CmdName[CMD_SESSION]);
const CmdDescriptor CmdQuit     (CmdName[CMD_QUIT]);
const CmdDescriptor CmdPing     (CmdName[CMD_PING]);
const CmdDescriptor CmdStates   (CmdName[CMD_STATES]);
const CmdDescriptor CmdProcHdl  (CmdName[CMD_PROCHDL]);
const CmdDescriptor CmdConfig   (CmdName[CMD_CONFIG]);
const CmdDescriptor CmdReproc   (CmdName[CMD_REPROC]);

//-- Processing Handling constants ------------

#undef T
#define TLISTOF_PROCHDL_SUBJECT_IDS                  \
    T(TASK), T(AGENT), T(HOST)

#define T(x) PROC_ ## x
enum SubjectId { TLISTOF_PROCHDL_SUBJECT_IDS };
#undef T

typedef QString SubcmdDescriptor;

#define TLISTOF_PROCHDL_SUBCMD_IDS                  \
    T(PAUSE), T(RESUME), T(CANCEL),                 \
    T(SUSPEND), T(STOP), T(REACTIVATE)

#define T(x) PROC_HDL_ ## x
enum SubcmdId { TLISTOF_PROCHDL_SUBCMD_IDS };
#undef T

#define T(x) QString( #x )
const SubcmdDescriptor SubcmdName[] = { TLISTOF_PROCHDL_SUBCMD_IDS };
#undef T

#endif
