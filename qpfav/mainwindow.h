#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QVector>
#include <QColor>
#include <QStatusBar>
#include <QListWidget>

#include <QStandardItemModel>

#include <QNetworkAccessManager>

#include "actionshandler.h"
#include "hmitypes.h"

class LocalArchiveView;
               
namespace Ui {
class MainWindow;
}

namespace SPF {

class LogSyntaxHighlighter;
    
class MainWindow : public QMainWindow
{
    Q_OBJECT

    // Status bar
    static const int MessageDelay;

    // URLs
    static const QString VOSpaceURL;

public:
    explicit MainWindow(QWidget *parent, QString & cfgFile, 
                        QString s = QString("cleanlooks"));
    ~MainWindow();

    void getProductTypes(std::vector<std::string> & pTypes, int & siz);

public slots:
    // File menu
    void quitSPFAV();
    void processPath();

    // Edit menu
    void cut();
    void copy();
    void paste();

    // View menu

    // Tools menu
    void showConfigTool();
    void showDBBrowser();
    void showExtToolsDef();
    void showVerbLevel();

    // Help menu
    void about();

    // Product Viewers
    void addToProdViewersList(QString);
    void addToViewsList(QString);
    void addToRptFiltList(QString);
    
    void getUserTools(MapOfUserDefTools & u);

    // Filtering 
    void reportFiltering();

    // Logs
    void showRequestedLog(int idx);
    void updateRequestedLog();
    void updateWrapLogMode();

private slots:
    QString getVar(QString var, QString filter);
    void setVar(QString var, QString filter, QString new_value);
    void run();

    void getMachinesStatus();
    void setProductsFilter(QString qry, QStringList hdr);
    void restartProductsFilter();

private:
    void configure(QString & cfgFile);
    void completeUi();
    void initLocalArchiveView(LocalArchiveView * v,
                              ActionsHandler * ah);
    void initProdViewFiltersFmk();
    void initPalette();
    void setDB();

    void getUserToolsFromSettings();
    void putUserToolsToSettings();

    void storeQUTools2Cfg(MapOfUserDefTools qutmap);
    void setUToolTasks(LocalArchiveView * v = nullptr);
    void showSection(int sec);

    void updateState();
    void updateVerbLogLevel();

    void newListWidgetSeparator(QListWidget * lwdg);

    void processProductsInPath(QString folder);
    void getProductsInFolder(QString & path, QStringList & files, bool recursive = true);
    
private:
    Ui::MainWindow *ui;

    ActionsHandler * aHdl;

    // User Defined Tools
    MapOfUserDefTools userDefTools;
    QStringList       userDefProdTypes;

    QString styleName;

    QStandardItemModel* Model;
    QStandardItem* Item1;
    QStandardItem* Item2;
 
    std::vector<QStandardItem*> Items;

    QString stateName;

    QNetworkAccessManager * netMng;

    LogSyntaxHighlighter * logHl;

    friend class ActionsHandler;

    bool isProductsCustomFilterActive;

    static const QStringList LocalArchiveViewFields;
    static const QStringList LocalArchiveViewFieldNames;
};

}

#endif // MAINWINDOW_H
