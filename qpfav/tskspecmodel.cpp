/******************************************************************************
 * File:    tskspecmodel.cpp
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.tskspecmodel
 *
 * Version:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Provides object implementation for some declarations
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/

#include "tskspecmodel.h"
#include "colors.h"

#include <iostream>

namespace SPF {

TaskSpectraModel::TaskSpectraModel()
{
    defineQuery(QString::fromStdString(
                "SELECT * FROM task_status_spectra;"));

    defineHeaders({"Host", "Agent", "Running", "Waiting", "Paused",
                   "Stopped", "Failed", "Finished", "Total"});

    refresh();
}

QVariant TaskSpectraModel::data(const QModelIndex &index, int role) const
{
    QVariant content = QStandardItemModel::data(index, role);

    if ((role == Qt::ForegroundRole) || (role == Qt::BackgroundRole)) {
        // Return fg/bg color 
        const FgBgColors & fb = taskSpectraPalette.at(index.column());
        return ((role == Qt::BackgroundRole) ?
                QVariant(fb.bgColor) :
                QVariant(fb.fgColor));
    } else if ((index.column() > 1) && (role == Qt::TextAlignmentRole)) {
        // Align to right number columns
        return Qt::AlignRight;
    } else if ((index.column() == 0) && (index.parent() != QModelIndex())) {
        // Do not show anything for first column below parent lines
        return QVariant();
    }
    
    return content;
}

}
