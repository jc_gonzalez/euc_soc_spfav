#ifndef FRM_LOCALARCHVIEW_H
#define FRM_LOCALARCHVIEW_H

#include <QWidget>

namespace Ui {
class FrmLocalArchView;
}

class FrmLocalArchView : public QWidget
{
    Q_OBJECT

public:
    explicit FrmLocalArchView(QWidget *parent = 0);
    ~FrmLocalArchView();

public:
    void setSql(QString qry);
    
public slots:
    void slotUpdatedSql();
    void slotCloseThisView();

signals:
    void closeThisViewRequested(QWidget*);
    
private:
    Ui::FrmLocalArchView * ui;
};

#endif // FRMLOCALARCHVIEW_H
