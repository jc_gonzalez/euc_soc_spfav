/******************************************************************************
 * File:    sysalertmodel.h
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.sysalertmodel
 *
 * Last update:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declaration of several datasysalertmodel for SPF HMI
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/
#ifndef SYSALERTMODEL_H
#define SYSALERTMODEL_H

#include "dbtblmodel.h"
#include "alert.h"

namespace SPF {

class SysAlertModel : public DBTableModel {

    Q_OBJECT

public:
    explicit SysAlertModel();

    virtual Alert getAlertAt(QModelIndex idx);
};

}

#endif // SYSALERTMODEL_H
