#include "frm_localarchview.h"
#include "ui_frm_localarchview.h"

#include "dbmng.h"

#include <QSqlQueryModel>

FrmLocalArchView::FrmLocalArchView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FrmLocalArchView)
{
    ui->setupUi(this);
}

FrmLocalArchView::~FrmLocalArchView()
{
    delete ui;
}

void FrmLocalArchView::slotUpdatedSql()
{
    setSql(ui->pltxtSql->toPlainText());
}

void FrmLocalArchView::slotCloseThisView()
{
    emit closeThisViewRequested(this);
}

void FrmLocalArchView::setSql(QString qry)
{
    if (ui->tblView->model() != 0) {
        delete ui->tblView->model();
    }
    QSqlQueryModel * model = new QSqlQueryModel;
    model->setQuery(qry, SPF::DBManager::getDB());

    ui->tblView->setModel(model);

    ui->pltxtSql->setPlainText(qry);
}
