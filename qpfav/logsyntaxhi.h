/******************************************************************************
 * File:    logsyntaxhi.h
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.logsyntaxhi
 *
 * Last update:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declaration of the class LogSyntaxHighlighter
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/
#ifndef LOGSYNTAXHI_H
#define LOGSYNTAXHI_H

#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QRegularExpression>

QT_BEGIN_NAMESPACE
class QTextDocument;
QT_END_NAMESPACE

namespace SPF {

class LogSyntaxHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    LogSyntaxHighlighter(QTextDocument *parent = 0);

protected:
    void highlightBlock(const QString &text) override;

private:
    struct HighlightingRule {
        QRegularExpression pattern;
        QTextCharFormat format;
    };
    QVector<HighlightingRule> highlightingRules;

    QTextCharFormat traceFmt;
    QTextCharFormat debugFmt;
    QTextCharFormat infoFmt;
    QTextCharFormat warnFmt;
    QTextCharFormat errorFmt;
    QTextCharFormat fatalFmt;
    QTextCharFormat dateFmt;
};

}

#endif // LOGSYNTAXHI_H
