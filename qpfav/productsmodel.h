/******************************************************************************
 * File:    productsmodel.h
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.productsmodel
 *
 * Last update:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declaration of several dataproductsmodel for SPF HMI
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/
#ifndef PRODUCTSMODEL_H
#define PRODUCTSMODEL_H

#include "dbtreemodel.h"

namespace SPF {

class ProductsModel : public DBTreeModel {

    Q_OBJECT

public:
    explicit ProductsModel(std::vector<std::string> & pTypes, 
			   int siz);

    int getNumFldPType();
    int getNumFldUrl();

public slots:
    void activateQuery(int i);

private:
    struct PQuery {
        QString qstr;
        QStringList hdrs;
        int skip;
        int fld_ptype;
        int fld_url;
    };

    void defineProductStrings(std::vector<std::string> & pTypes, int siz);
    void defineQueries();
 
    std::string stringWithProductTypes;
    int singleProdTypeLen;

    QVector<PQuery> query;    
    int numFldPType;
    int numFldUrl;

};

}

#endif // PRODUCTSMODEL_H
