/******************************************************************************
 * File:    fmkavconn.cpp
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.fmkavconn
 *
 * Version:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Provides object implementation for some declarations
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/

#include "fmkavconn.h"

#include <iostream>

namespace SPF {

QSqlDatabase       FmkAVConnection::db;

//----------------------------------------------------------------------
// Constructor
//----------------------------------------------------------------------
FmkAVConnection::FmkAVConnection()
{
}

//----------------------------------------------------------------------
// Destructor
//----------------------------------------------------------------------
FmkAVConnection::~FmkAVConnection()
{
}

//----------------------------------------------------------------------
// Method: setDB
// setDB
//----------------------------------------------------------------------
void FmkAVConnection::setDB(QSqlDatabase & aDB)
{
    db = aDB;
}

//----------------------------------------------------------------------
// Method: addICmd
// addICmd
//----------------------------------------------------------------------
void FmkAVConnection::addICmd(QString src, QString tgt, QString cmd)
{
    QDateTime now(QDateTime::currentDateTime());
    QDateTime nowUTC(now);
    nowUTC.setTimeSpec(Qt::UTC);

    QString sqry(QString("INSERT INTO icommands "
                         "(cmd_date, cmd_source, cmd_target, cmd_executed, cmd_content) "
                         "VALUES ('%1', '%2', '%3', false, '%4');")
                 .arg(nowUTC.toString(Qt::ISODate))
                 .arg(src)
                 .arg(tgt)
                 .arg(cmd));
    QSqlQuery qry(sqry, db);

    std::cerr << sqry.toStdString() << '\n';

    if (qry.lastError().type() != QSqlError::NoError) {
        qErrnoWarning(qPrintable(qry.lastError().nativeErrorCode() + ": " +
                                 qry.lastError().text()));
    }
}

//----------------------------------------------------------------------
// Method: getICmd
// getICmd
//----------------------------------------------------------------------
bool FmkAVConnection::getICmd(QString cmd, bool removeCmd)
{
    bool result = true;
    QString sqry(QString("SELECT cmd.id "
                         " FROM icommands cmd "
                         " WHERE cmd.cmd_source = '%1' "
                         "   AND cmd.cmd_target = '%2'"
                         "   AND cmd.cmd_content = '%3'"
                         "   AND cmd.cmd_executed = false "
                         " ORDER BY cmd.id LIMIT 1;")
                 .arg("EvtMng")
                 .arg("SPFHMI")
                 .arg(cmd));
    QSqlQuery qry(sqry, db);
    if (qry.next()) {
        QString id(qry.value(0).toString());
        if (removeCmd) {
           // Remove command from table
            QSqlQuery qry2(QString("DELETE FROM icommands "
                                   " WHERE id = %1;").arg(id), db);
        } else {
            // Deactivate answer, so it doesn't get used again
            QSqlQuery qry2(QString("UPDATE icommands SET cmd_executed = true "
                                   " WHERE id = %1;").arg(id), db);
        }
    } else if (qry.lastError().type() != QSqlError::NoError) {
        qErrnoWarning(qPrintable(qry.lastError().nativeErrorCode() + ": " +
                                 qry.lastError().text()));
        result = false;
    } else {
        result = false;
    }

    return result;
}

//----------------------------------------------------------------------
// Method: rmICmd
// removeICmds
//----------------------------------------------------------------------
void FmkAVConnection::rmICmd(QString cmd)
{
    QString sqry(QString("DELETE FROM icommands cmd "
                         " WHERE cmd.cmd_content = '%1';").arg(cmd));
    QSqlQuery qry(sqry, db);
}

//----------------------------------------------------------------------
// Method: sendProcHdlCmd
// sendProcHdlCmd
//----------------------------------------------------------------------
void FmkAVConnection::sendProcHdlCmd(SubjectId subj, QString subjName, SubcmdId subCmd)
{
    QString body(QString("\"body\": {\"cmd\": \"%1\", "
			 "\"subcmd\": \"%2\", "
			 "\"target_type\": %3, "
			 "\"target\": \"%4\"}")
		 .arg(CmdProcHdl)
		 .arg(SubcmdName[(int)(subCmd)])
		 .arg((int)(subj))
		 .arg(subjName));
    QString hdr("\"header\": {\"dateCreation\":\"\",\"dateReception\":\"\","
		"\"dateTransmission\":\"\",\"timeStamp\":\"\","
		"\"id\":\"HMICMD\",\"source\":\"HMIProxy\",\"target\":\"*\","
		"\"type\":\"HMICMD\",\"version\":\"1.0\"}");

    QString content(QString("{%1, %2}").arg(hdr).arg(body));
	
    addICmd("SPFAV", "SPF", content);
}

}
