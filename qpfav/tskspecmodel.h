/******************************************************************************
 * File:    tskspecmodel.h
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.tskspecmodel
 *
 * Last update:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declaration of several datatskspecmodel for SPF HMI
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/
#ifndef TSKSPECMODEL_H
#define TSKSPECMODEL_H

#include "dbtreemodel.h"

namespace SPF {

class TaskSpectraModel : public DBTreeModel {

    Q_OBJECT

public:
    explicit TaskSpectraModel();

    QVariant data(const QModelIndex &index, int role) const;

};

}

#endif // TSKSPECMODEL_H
