/******************************************************************************
 * File:    tasksmodel.cpp
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.tasksmodel
 *
 * Version:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Provides object implementation for some declarations
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/

#include "tasksmodel.h"
#include "colors.h"

namespace SPF {

TasksModel::TasksModel()
{
    defineQuery("SELECT "
                "id as ID, "
                "t.task_data#>>'{State,StartedAt}' AS start, "
                "t.task_data#>>'{State,FinishedAt}' AS finish, "
                "t.task_data#>>'{IO,input}' AS name, "
                "SUBSTRING(t.task_path, 10, 14) AS agent, "
                "t.task_path AS proc, "
                "tt.status_desc AS status, "
                "t.task_progress AS progress, "
                "t.task_data#>>'{State,ExitCode}' AS exit_code, "
                "t.task_data AS task_data "
                "FROM tasks_info t "
                "INNER JOIN task_status tt "
                "      ON t.task_status_id = tt.task_status_id "
                "ORDER BY id;");

    defineHeaders({"ID", "Started at", "Finished at",
                "Main Input Product", "Agent", "Working Dir.",
                "Status", "Progress", "Exit Code", "Task Info"});

    TablePalette  tblPalette;
    tblPalette[6] = statusPalette;

    defineTablePalette(tblPalette);

    setFullUpdate(true);

    refresh();
}

}
