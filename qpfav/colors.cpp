/******************************************************************************
 * File:    colors.cpp
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.colors
 *
 * Last update:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Definition of some particular colors for model data
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/

#include "colors.h"

namespace SPF {

    FgBgColors TaskMdlColorScheduled(QColor(Qt::gray),  QColor(255, 255, 255, 0));
    FgBgColors TaskMdlColorRunning(QColor(Qt::darkGreen), QColor(255, 255, 255, 0));
    FgBgColors TaskMdlColorFinished(QColor(Qt::black), QColor(Qt::green));
    FgBgColors TaskMdlColorPaused(QColor(Qt::blue),  QColor(Qt::lightGray));
    FgBgColors TaskMdlColorFailed(QColor(Qt::white), QColor(Qt::red));
    FgBgColors TaskMdlColorStopped(QColor(Qt::black), QColor(Qt::yellow));
    FgBgColors TaskMdlColorAborted(QColor(Qt::black), QColor(Qt::darkYellow));
    FgBgColors TaskMdlColorArchived(QColor(Qt::black), QColor(Qt::gray));
    FgBgColors TaskMdlColorUnknown(QColor(Qt::red),   QColor(Qt::darkRed));

    ColumnPalette statusPalette = { {"SCHEDULED", TaskMdlColorScheduled},
                                    {"RUNNING",   TaskMdlColorRunning},
                                    {"FINISHED",  TaskMdlColorFinished},
                                    {"PAUSED",    TaskMdlColorPaused},
                                    {"FAILED",    TaskMdlColorFailed},
                                    {"STOPPED",   TaskMdlColorStopped},
                                    {"ABORTED",   TaskMdlColorAborted},
                                    {"ARCHIVED",  TaskMdlColorArchived},
                                    {"UNKNOWN",   TaskMdlColorUnknown} };

    FgBgColors TaskSpecColorScheduled(QColor(Qt::gray),  QColor(255, 255, 255, 0));
    FgBgColors TaskSpecColorRunning(QColor(Qt::blue), QColor(255, 255, 255, 0));
    FgBgColors TaskSpecColorFinished(QColor(Qt::darkGreen), QColor(255, 255, 255, 0));
    FgBgColors TaskSpecColorPaused(QColor(Qt::magenta), QColor(255, 255, 255, 0));
    FgBgColors TaskSpecColorFailed(QColor(Qt::red), QColor(255, 255, 255, 0));
    FgBgColors TaskSpecColorStopped(QColor(Qt::darkYellow), QColor(255, 255, 255, 0));
    FgBgColors TaskSpecColorAborted(QColor(Qt::darkMagenta), QColor(255, 255, 255, 0));
    FgBgColors TaskSpecColorArchived(QColor(Qt::black), QColor(255, 255, 255, 0));
    FgBgColors TaskSpecColorUnknown(QColor(Qt::red), QColor(255, 255, 255, 0));

    FgBgColors TaskSpecColorTotal(QColor(Qt::black), QColor(Qt::lightGray));

    FgBgColors TaskSpecColorHost(QColor(Qt::white),  QColor(Qt::darkBlue));
    FgBgColors TaskSpecColorAgent(QColor(Qt::lightGray),  QColor(Qt::darkCyan));

    TaskSpectraPalette taskSpectraPalette( { TaskSpecColorHost,
                                             TaskSpecColorAgent,
                                             TaskSpecColorRunning,        
                                             TaskSpecColorScheduled,
                                             TaskSpecColorPaused,
                                             TaskSpecColorStopped,
                                             TaskSpecColorFailed,
                                             TaskSpecColorFinished,
                                             TaskSpecColorTotal} );
    
}


