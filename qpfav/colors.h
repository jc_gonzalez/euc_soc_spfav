/******************************************************************************
 * File:    colors.h
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.colors
 *
 * Last update:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declaration of several elements to handle colors for model data
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/
#ifndef COLORS_H
#define COLORS_H

#include <QMap>
#include <QVector>
#include <QColor>

namespace SPF {

    struct FgBgColors {
        FgBgColors(QColor fg = QColor(), QColor bg = QColor()) : fgColor(fg), bgColor(bg) {}
        QColor fgColor;
        QColor bgColor;
    };

    typedef QMap<QString, FgBgColors> ColumnPalette;
    typedef QMap<int, ColumnPalette>  TablePalette;

    typedef QVector<FgBgColors> TaskSpectraPalette;

    extern ColumnPalette statusPalette;
    extern TaskSpectraPalette taskSpectraPalette;
}

#endif // COLORS_H
