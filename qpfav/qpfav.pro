#----------------------------------------------------------------------
# SPFAV - SPF Archive Viewer
#----------------------------------------------------------------------
# Project file for the compilation of the SPFAV executable
# using Qt's QMake
#
# Created by J C Gonzalez
# Copyright (C) 2018-2019 by the Euclid SOC Team at ESAC
#----------------------------------------------------------------------

TARGET = qpfav
VERSION = "2.1"

TEMPLATE = app
QT       += core gui sql network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += ordered c++11

DESTDIR = $$PWD/../build/qpfav.dir
OBJECTS_DIR = $$DESTDIR/obj
MOC_DIR = $$DESTDIR/moc
UI_DIR = $$DESTDIR/ui
RCC_DIR = $$DESTDIR/rcc

MAINGUI  = mainwindow

WIDGETS = \
    configtool		\
    conthostedit	\
    frm_diagflt         \
    frm_filter          \
    frm_filterset       \
    frm_filtview        \
    frm_localarchview   \
    localarchview	\		
    procalertsview	\
    productsviewer	\
    qjsonviewer		\
    ruleedit		\
    statelbl 		\
    swarmedit		\
    sysalertsview	\			
    tasksview		

DIALOGS = \
    dbbrowser		\
    dlgalert		\
    dlgjsonviewer	\
    dlgqdtfilter        \
    dlgreproc		\
    dlguserpwd  	\
    exttooledit		\
    exttoolsdef		\
    verbleveldlg	

UIS = $$MAINGUI $$WIDGETS $$DIALOGS

CLASSES = \
    actionshandler	\
    alert		\
    browser		\
    colors              \
    config		\
    connectionwidget	\
    darkpalette		\
    dbmng		\
    dbtblmodel		\
    dbtreemodel		\
    filetools		\
    fmkavconn           \
    keyevt		\
    launcher		\
    logsyntaxhi         \
    procalertmodel	\
    progbardlg          \
    prodfiltmodel       \
    productsmodel	\
    qdtfilters          \
    qjsonitem		\
    qjsonmodel		\
    rwc			\
    sysalertmodel	\
    tasksmodel		\
    tskspecmodel        \
    util		\
    voshdl		\
    xmlsyntaxhighlight	

ADD_SOURCES = main.cpp

ADD_HEADERS = scopeexit.h cmd.h filters.h

ADD_FORMS = browserwidget.ui

RESOURCES += icons_nav.qrc states.qrc

#----------------------------------------------------------------------

ALL_SRC_FILES = $$UIS $$CLASSES
ALL_HDR_FILES = $$UIS $$CLASSES

#----------------------------------------------------------------------

SOURCES += $$split($$list($$join(ALL_SRC_FILES, ".cpp ", "", ".cpp")), " ") $$ADD_SOURCES

HEADERS += $$split($$list($$join(ALL_HDR_FILES, ".h ", "", ".h")), " ")     $$ADD_HEADERS

FORMS   += $$split($$list($$join(UIS, ".ui ", "", ".ui")), " ") $$ADD_FORMS

DISTFILES +=

INCLUDEPATH += $$PWD/../qslog 
LIBS += -L$$PWD/../build/qslog.dir -lqslog -lcurl -L/lib64 -lX11
DEFINES += QSLOG_IS_SHARED_LIBRARY_IMPORT
