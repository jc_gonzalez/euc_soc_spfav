#ifndef TASKSVIEW_H
#define TASKSVIEW_H

#include <QWidget>
#include <QMap>

#include "tasksmodel.h"
#include "mainwindow.h"
#include "actionshandler.h"
#include "util.h"

#include "progbardlg.h"

using SPF::MainWindow;
using SPF::ActionsHandler;
using SPF::ProgressBarDelegate;

namespace Ui {
class TasksView;
}

using SPF::TasksModel;

class TasksView : public QWidget
{
    Q_OBJECT

public:
    explicit TasksView(QWidget *parent = 0);
    ~TasksView();

public slots:
    void setActionsHandler(ActionsHandler * a);

    void showWorkDir();
    void displayTaskInfo();
    void stopTask();
    void restartTask();
    void doTaskPause();
    void doTaskResume();
    void doTaskCancel();
    void doAgentSuspend();
    void doAgentStop();
    void doAgentReactivate();
    void doHostSuspend();
    void doHostStop();
    void doHostReactivate();

    void toggleAutoUpdate(bool tog);
    void run();
    void arefresh();

private slots:
    void temporallyDisableUpdate(const QPoint &);

private:
    Ui::TasksView * ui;

    TasksModel * model;
    MainWindow * mw;
    ActionsHandler * aHdl;

    QMap<QString,TaskStatus>  agentProcStatus;
    QMap<QString,TaskStatus>  hostProcStatus;

    ProgressBarDelegate * progressBarDisplay;

    bool autoUpdate;

    static const int NumOfProgBarCol;

};

#endif // TASKSVIEW_H
