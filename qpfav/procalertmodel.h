/******************************************************************************
 * File:    procalertmodel.h
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.procalertmodel
 *
 * Last update:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declaration of several dataprocalertmodel for SPF HMI
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/
#ifndef PROCALERTMODEL_H
#define PROCALERTMODEL_H

#include "dbtblmodel.h"

namespace SPF {

class ProcAlertModel : public DBTableModel {

    Q_OBJECT

public:
    explicit ProcAlertModel();

    virtual Alert getAlertAt(QModelIndex idx);
};

}

#endif // PROCALERTMODEL_H
