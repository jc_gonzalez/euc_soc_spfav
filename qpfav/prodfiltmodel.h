/******************************************************************************
 * File:    prodfiltmodel.h
 *          This file is part of SOC Processing Framework
 *
 * Domain:  SPF.libSPF.prodfiltmodel
 *
 * Last update:  2.0
 *
 * Date:    2015/07/01
 *
 * Author:   J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declaration of several ProductsFiltModel for SPF HMI
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   none
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog>
 *
 * About: License Conditions
 *   See <License>
 *
 ******************************************************************************/
#ifndef PRODFILTMODEL_H
#define PRODFILTMODEL_H

#include "dbtreemodel.h"

namespace SPF {

class ProductsFilterModel : public DBTreeModel {

    Q_OBJECT

public:
    explicit ProductsFilterModel(QString qry);

public slots:
    void changeQuery(QString qry);
};

}

#endif // PRODFILTMODEL_H
